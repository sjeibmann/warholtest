import blobDetection.*;
import processing.video.*;

PImage source;       // Source image
PImage destination;  // Destination image

Capture cam;

XML xml;
XML[] xmlList;
String[] colors = new String[54];
int colorLocation = 0;
color bg;

float thres = 0.4;

void setup() {
  size(640, 300, P2D);
  xml = loadXML("colors.xml");
  xmlList = xml.getChildren("color");
  for(int i=0; i<xmlList.length;i++){
    //println(i + ": " + xmlList[i].getContent());
    colors[i] = xmlList[i].getContent();
  }
  
  setBGColor();
  String[] cameras = Capture.list();
  //if (cameras.length == 0) {
  // println("There are no cameras available for capture.");
  // exit();
  //} else {
  // println("Available cameras:");
  // for (int i = 0; i < cameras.length; i++) {
  //   println(cameras[i]);
  // }
  //}
  println(cameras[1]);
  cam = new Capture(this, cameras[1]);
  cam.start();
}

void draw(){
  background(bg);
  if (cam.available() == true) {
    cam.read();
  }
    cam.loadPixels();
    //.loadPixels();
    //filter(THRESHOLD,thres);
    removeImageWhite(cam);
    scale(0.5);
    image(cam, 0, 0);
}

void removeImageWhite(PImage camImg){
  for(int x = 0; x < camImg.width ; x++){
    for(int y = 0; y < camImg.height ; y++){
      int loc = x + y*camImg.width;
      //if(camImg.pixels[loc] == 0xFFFFFFFF ){
      if(brightness(camImg.pixels[loc]) > (thres*255)){
        cam.pixels[loc] &= 0x00FFFFFF;
        //println("bloop");
        //cam.pixels[loc] = color(255,45,129);
      }else{
        cam.pixels[loc] = color(0);
      }
    }
  }
  cam.updatePixels();
}


void setBGColor(){
  int colorValue = unhex(colors[colorLocation]);
  bg = colorValue;
}

void keyPressed(){
  switch(keyCode){
    case '.':
      thres = thres + 0.01;
      println("threshold: " + thres);
      break;
    case ',':
      thres = thres - 0.01;
      println("threshold: " + thres);
      break;
    case 39: //right key
      if(colorLocation < xmlList.length-1){
        colorLocation += 1;
      }else{
        colorLocation = 0;
      }
      println("color Value: " + colorLocation + ") " + colors[colorLocation]);
      setBGColor();
      break;
    case 37: //left key
      if(colorLocation > 0){
        colorLocation -= 1;
      }else{
        colorLocation = xmlList.length-1;
      }
      println("color Value: " + colorLocation + ") " + colors[colorLocation]);
      setBGColor();
      break;
    default:
      println("No case matches: " + keyCode);
      break;
  }
}